import pygame

pygame.init()

class Juego():
    frames_pelota= ̣̣̣̣   {
        0: (414, 55, 25, 27)
    }
    frames_aro= {
        0: (0, 154, 23, 23),
        1: (28, 154, 22, 23),
        2: (55, 154, 25, 21),
        3: (84, 154, 23, 23)
    }
    ventana= pygame.display.set_mode([714,477]) #crear ventana
    fondo= None
    velocidad= 8
    pygame.display.set_caption("Basketball project") #Nombre de la ventana

    def cargar_imagenes(self):
        self.ruta_imagen = "assets/spritesheets/fondoPrincipal.jpg"
        self.ancho_deseado = 714
        self.alto_deseado = 477
        self.imagen_original = pygame.image.load(self.ruta_imagen).convert()
        self.imagen_redimensionada = pygame.transform.scale(self.imagen_original, (self.ancho_deseado, self.alto_deseado))
   
    def iniciar_juego(self):
        while True:
            self.ventana.blit(self.imagen_redimensionada, (0, 0))
            pygame.display.flip()


juego= Juego()
juego.cargar_imagenes()
juego.iniciar_juego()